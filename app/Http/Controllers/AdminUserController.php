<?php

namespace App\Http\Controllers;

use App\Http\Requests\AdminUserController\LoginRequest;
use App\Interfaces\AdminUserControllerInterface;
use App\Traits\SendApiResponseTrait;
use Illuminate\Http\Request;

class AdminUserController extends Controller
{
    use SendApiResponseTrait;

    /**
     * @var AdminUserControllerInterface
     */
    private AdminUserControllerInterface $adminUserController;

    /**
     * AdminUserController constructor.
     * @param AdminUserControllerInterface $adminUserController
     */
    public function __construct(AdminUserControllerInterface $adminUserController)
    {
        $this->adminUserController = $adminUserController;
    }

    public function login(LoginRequest $request)
    {
        try {
            $token = $this->adminUserController->login($request);
            if ($token) {
                return $this->successResponse([
                    'token' => $token
                ], 'Admin Authentication Successful', 200);
            }
            return $this->failureResponse('Incorrect email or password', 401);
        } catch (\Exception $exception) {
            return $this->failureResponse($exception->getMessage(), $exception->getCode());
        }
    }
}
