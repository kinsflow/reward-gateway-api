<?php

namespace App\Http\Controllers;

use App\Http\Requests\ProductInformationController\StoreRequest;
use App\Http\Requests\ProductInformationController\UpdateRequest;
use App\Interfaces\ProductInformationControllerInterface;
use App\Models\Product;
use App\Models\Supplier;
use App\Traits\SendApiResponseTrait;
use Illuminate\Http\Request;

class ProductInformationController extends Controller
{
    use SendApiResponseTrait;

    /**
     * @var ProductInformationControllerInterface
     */
    private ProductInformationControllerInterface $productInformationController;

    /**
     * ProductInformationController constructor.
     * @param ProductInformationControllerInterface $productInformationController
     */
    public function __construct(ProductInformationControllerInterface $productInformationController)
    {
        $this->productInformationController = $productInformationController;
    }

    /**
     * Return all Products
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function index()
    {
        return $this->successResponse($this->productInformationController->index());
    }

    /**
     * Return a Product
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function show(Product $product)
    {
        return $this->successResponse($product);
    }

    /**
     * store a new product information
     * @param StoreRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function store(StoreRequest $request)
    {
        abort_if(!auth('admin')->check(), 401, 'Unauthorized');
        try {
            $product = $this->productInformationController->store($request);
            return $this->successResponse($product, 'Product Saved Successfully', 201);
        } catch (\Exception $exception) {
            return $this->failureResponse($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * update a product information
     * @param UpdateRequest $request
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function update(UpdateRequest $request, Product $product)
    {
        abort_if(!auth('admin')->check(), 401, 'Unauthorized');
        try {
            $this->productInformationController->update($request, $product);

            $product->refresh();
            return $this->successResponse($product, 'Product Updated Successfully', 200);
        } catch (\Exception $exception) {
            return $this->failureResponse($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * delete a product
     * @param Product $product
     * @return \Illuminate\Http\JsonResponse
     */
    public function destroy(Product $product)
    {
        $product->delete();
        return $this->successResponse('', 'Product Deleted Successfully', 204);
    }

    public function suppliers()
    {
        return $this->successResponse($this->productInformationController->suppliers(), 'Suppliers Returned Successfully', 200);
    }
}
