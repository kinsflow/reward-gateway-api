<?php

namespace App\Http\Controllers;

use App\Http\Requests\UserController\LoginRequest;
use App\Http\Requests\UserController\RegisterRequest;
use App\Interfaces\UserControllerInterface;
use App\Traits\SendApiResponseTrait;

class UserController extends Controller
{
    use  SendApiResponseTrait;

    private UserControllerInterface $userController;

    /**
     * UserController constructor.
     * @param UserControllerInterface $userController
     */
    public function __construct(UserControllerInterface $userController)
    {
        $this->userController = $userController;
    }

    /**
     * Register User Into the Platform
     * @param RegisterRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function register(RegisterRequest $request)
    {
        try {
            $user = $this->userController->register($request);

            return $this->successResponse($user, 'User Registered Successful', 201);
        } catch (\Exception $exception) {
            return $this->failureResponse($exception->getMessage(), $exception->getCode());
        }
    }

    /**
     * Log user into this platform
     * @param LoginRequest $request
     * @return \Illuminate\Http\JsonResponse
     */
    public function login(LoginRequest $request)
    {
        try {
            $token = $this->userController->login($request);

            if ($token) {
                return $this->successResponse([
                    'token' => $token
                ], 'User Authentication Successful', 200);
            }
            return $this->failureResponse('Incorrect email or password', 401);
        } catch (\Exception $exception) {
            return $this->failureResponse($exception->getMessage(), $exception->getCode());
        }
    }
}
