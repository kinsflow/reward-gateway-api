<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;

class CheckAdminToken
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle(Request $request, Closure $next)
    {
        $request->headers->set('Accept', 'application/json');

        if (!$request->hasHeader('authorization')) {
            throw new \Exception('Authorization Required', 401);
        }

        if (!auth('admin')->check()) {
            throw new \Exception('Unauthorized', 401);
        }

        return $next($request);
    }
}
