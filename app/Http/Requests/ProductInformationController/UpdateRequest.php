<?php

namespace App\Http\Requests\ProductInformationController;

use Illuminate\Foundation\Http\FormRequest;

class UpdateRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return auth('admin')->check();
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        return [
            'supplier_id' => 'sometimes|integer|exists:suppliers,id',
            'name' => 'sometimes|string',
            'price' => 'sometimes|numeric'
        ];
    }
}
