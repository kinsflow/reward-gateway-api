<?php

namespace App\Interfaces;

use Illuminate\Http\Request;

interface AdminUserControllerInterface
{
    public function login(Request $request);
}
