<?php

namespace App\Interfaces;

use App\Models\Product;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

interface ProductInformationControllerInterface {
    public function store(Request $request);

    public function update(Request $request, Product $model);

    public function show(Request $request);

    public function index();

    public function destroy(Request $request);

    public function suppliers();
}
