<?php

namespace App\Interfaces;

use Illuminate\Http\Request;
use Illuminate\Http\Response;

interface UserControllerInterface
{
    public function register(Request $request);

    public function login(Request $request);
}
