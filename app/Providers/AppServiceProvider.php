<?php

namespace App\Providers;

use App\Interfaces\AdminUserControllerInterface;
use App\Interfaces\ProductInformationControllerInterface;
use App\Interfaces\UserControllerInterface;
use App\Repositories\AdminUserControllerRepository;
use App\Repositories\ProductInformationControllerRepository;
use App\Repositories\UserControllerRepository;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        $this->app->bind(UserControllerInterface::class, UserControllerRepository::class);
        $this->app->bind(AdminUserControllerInterface::class, AdminUserControllerRepository::class);
        $this->app->bind(ProductInformationControllerInterface::class, ProductInformationControllerRepository::class);
    }
}
