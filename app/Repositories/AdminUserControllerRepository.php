<?php

namespace App\Repositories;

use App\Interfaces\AdminUserControllerInterface;
use App\Models\Admin;
use Illuminate\Http\Request;

class AdminUserControllerRepository implements AdminUserControllerInterface
{

    /**
     * @var Admin
     */
    private Admin $admin;

    /**
     * AdminUserControllerRepository constructor.
     * @param Admin $admin
     */
    public function __construct(Admin $admin)
    {
        $this->admin = $admin;
    }

    /**
     * Log Admin In to the system
     * @param Request $request
     * @return bool
     */
    public function login(Request $request)
    {
        return auth('admin')->attempt([
            'email' => $request->email,
            'password' => $request->password
        ]);
    }
}
