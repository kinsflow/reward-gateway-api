<?php


namespace App\Repositories;


use App\Interfaces\ProductInformationControllerInterface;
use App\Models\Product;
use App\Models\Supplier;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Http\Request;

class ProductInformationControllerRepository implements ProductInformationControllerInterface
{
    /**
     * @var Product
     */
    private Product $product;

    /**
     * ProductInformationControllerRepository constructor.
     * @param Product $product
     */
    public function __construct(Product $product)
    {
        $this->product = $product;
    }

    public function store(Request $request)
    {
        return $this->product->create([
            'supplier_id' => $request->supplier_id,
            'name' => $request->name,
            'price' => $request->price
        ]);
    }

    public function show(Request $request)
    {
        // TODO: Implement show() method.
    }

    public function index()
    {
        return Product::with('supplier:id,name')->get();
    }

    public function destroy(Request $request)
    {
        // TODO: Implement destroy() method.
    }

    public function update(Request $request, Product $product)
    {
        return $product->update($request->all());
    }

    public function suppliers()
    {
        return Supplier::all();
    }
}
