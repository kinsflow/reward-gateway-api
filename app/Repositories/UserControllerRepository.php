<?php

namespace App\Repositories;

use App\Interfaces\UserControllerInterface;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Http\Response;
use Illuminate\Support\Facades\Auth;

class UserControllerRepository implements UserControllerInterface
{
    /**
     * @var User
     */
    private User $user;

    /**
     * UserControllerRepository constructor.
     * @param User $user
     */
    public function __construct(User $user)
    {
        $this->user = $user;
    }

    /**
     * Register user into the platform
     * @param Request $request
     * @return Response
     */
    public function register(Request $request)
    {
        return $this->user->create([
            'first_name' => $request->first_name,
            'last_name' => $request->last_name,
            'email' => $request->email,
            'password' => bcrypt($request->password)
        ]);
    }

    /**
     * log user into the platform
     * @param Request $request
     * @return bool
     */
    public function login(Request $request)
    {
        return auth('user')->attempt([
            'email' => $request->email,
            'password' => $request->password
        ]);
    }
}
