<?php
namespace App\Traits;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Response;

trait SendApiResponseTrait {
    /**
     * Handles response for successful requests
     *
     * @param $data
     * @param string|null $message
     * @param bool $paginatedData
     * @return JsonResponse
     */
    public function successResponse($data = [], string $message = null, $code = null): JsonResponse
    {
        $array = [
            'message' => $message ?? 'Successful',
            'data' => $data
        ];

        return response()->json($array, $code ?? Response::HTTP_OK);
    }

    /**
     * Handles response for failed requests
     *
     * @param string $message
     * @param int|null $code
     * @param array $data
     * @return JsonResponse
     */
    public function failureResponse(string $message = null, int $code = null, array $data = []): JsonResponse
    {
        $statusCode = (empty($code) || $code == 0 || strlen($code) !== 3) ? Response::HTTP_BAD_REQUEST : $code;

        return response()->json([
            'message' => $message ?? 'Request failed',
            'data' => $data
        ], $statusCode);
    }
}
