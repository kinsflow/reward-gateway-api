<?php

namespace Database\Seeders;

use App\Models\Admin;
use Illuminate\Database\Seeder;

class AdminUserTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Admin::create([
            'first_name' => 'adminFirstName',
            'last_name' => 'adminLastName',
            'email' => 'admin@rewardgateway.com',
            'password' => bcrypt('password')
        ]);
    }
}
