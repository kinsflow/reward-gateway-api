<?php

namespace Database\Seeders;

use App\Models\Supplier;
use Illuminate\Database\Seeder;

class SupplierSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $supplier_names = [
            'Amadi & sons', 'Ekeji Ventures'
        ];

        foreach ($supplier_names as $name) {
            Supplier::create([
                'name' => $name
            ]);
        }
    }
}
