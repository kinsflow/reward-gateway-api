<?php

use App\Http\Controllers\AdminUserController;
use App\Http\Controllers\ProductInformationController;
use App\Http\Controllers\UserController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::middleware(['cors'])->group(function () {

    Route::prefix('user')->group(function () {
        Route::post('/login', [UserController::class, 'login']);
        Route::post('/register', [UserController::class, 'register']);
    });

    Route::prefix('admin')->group(function () {
        Route::post('/login', [AdminUserController::class, 'login']);
    });

    Route::prefix('products')->group(function () {
        Route::post('/', [ProductInformationController::class, 'store'])->middleware('auth:admin');
        Route::get('/', [ProductInformationController::class, 'index'])->middleware('auth:admin,user');
        Route::put('/{product}', [ProductInformationController::class, 'update'])->middleware('auth:admin');
        Route::get('/{product}', [ProductInformationController::class, 'show'])->middleware('auth:admin,user');
        Route::delete('/{product}', [ProductInformationController::class, 'destroy'])->middleware('auth:admin');
        Route::delete('/{product}', [ProductInformationController::class, 'destroy'])->middleware('auth:admin');
    });
    Route::get('/suppliers', [ProductInformationController::class, 'suppliers'])->middleware('auth:admin');
});
