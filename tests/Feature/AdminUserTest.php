<?php

namespace Tests\Feature;

use Illuminate\Http\Response;
use Tests\TestCase;

class AdminUserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_login()
    {
        $response = $this->withHeader('Accept', 'application/json')->json('post', 'api/admin/login/', [
            'email' => 'admin@rewardgateway.com',
            'password' => 'password'
        ]);

        $response->assertStatus(Response::HTTP_OK);
    }
}
