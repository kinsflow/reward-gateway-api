<?php

namespace Tests\Feature;

use App\Models\Product;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class ProductTest extends TestCase
{
    /**
     * This test is intended to test whether the creation
     * of product as a feature is well functional (by the admin)
     */
    public function test_create_product()
    {
        $admin_token = auth('admin')->attempt([
            'email' => 'admin@rewardgateway.com',
            'password' => 'password'
        ]);

        if ($admin_token) {
            $response = $this->withHeader('Authorization', 'Bearer' . $admin_token)
                ->json('post', '/api/products', [
                    "name" => "Jumoke",
                    "price" => 500.00,
                    "supplier_id" => 1
                ]);
            $response->assertStatus(Response::HTTP_CREATED);
        }
    }

    /**
     * test endpoint that update a product
     */
    public function test_update_product()
    {
        $admin_token = auth('admin')->attempt([
            'email' => 'admin@rewardgateway.com',
            'password' => 'password'
        ]);

        $product = Product::first();

        if ($admin_token) {
            $response = $this->withHeader('Authorization', 'Bearer' . $admin_token)
                ->json('put', '/api/products/' . $product->id, [
                    "name" => "Jumoke",
                    "price" => 500.00,
                    "supplier_id" => 1
                ]);
            $response->assertStatus(Response::HTTP_OK);
        }
    }

    /**
     * test endpoint that returns all products
     */
    public function test_return_products()
    {
        $admin_token = auth('admin')->attempt([
            'email' => 'admin@rewardgateway.com',
            'password' => 'password'
        ]);

        if ($admin_token) {
            $response = $this->withHeader('Authorization', 'Bearer' . $admin_token)
                ->json('get', '/api/products');
            $response->assertStatus(Response::HTTP_OK);
        }
    }

    /**
     * test endpoint that returns a product
     */
    public function test_return_a_product()
    {
        $admin_token = auth('admin')->attempt([
            'email' => 'admin@rewardgateway.com',
            'password' => 'password'
        ]);
        $product = Product::first();
        if ($admin_token) {
            $response = $this->withHeader('Authorization', 'Bearer' . $admin_token)
                ->json('get', '/api/products/' . $product->id);
            $response->assertStatus(Response::HTTP_OK);
        }
    }

    public function test_return_suppliers()
    {
        $admin_token = auth('admin')->attempt([
            'email' => 'admin@rewardgateway.com',
            'password' => 'password'
        ]);

        if ($admin_token) {
            $response = $this->withHeader('Authorization', 'Bearer' . $admin_token)
                ->json('get', '/api/suppliers');
            $response->assertStatus(Response::HTTP_OK);
        }
    }
}
