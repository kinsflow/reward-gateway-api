<?php

namespace Tests\Feature;

use App\Models\User;
use Symfony\Component\HttpFoundation\Response;
use Tests\TestCase;

class UserTest extends TestCase
{
    /**
     * A basic feature test example.
     *
     * @return void
     */
    public function test_register()
    {
        $user = User::firstOrFail();
        if ($user) {
            $user->delete();
        }

        $user_payload = [
            "email" => "kingsley.davidakindele@gmail.com",
            "password" => "Password",
            "first_name" => "Kingsley",
            "last_name" => "Akindele"
        ];

        $response = $this->json('post', '/api/user/register', $user_payload);

        $response->assertStatus(Response::HTTP_CREATED);
    }

    public function test_login()
    {
        $user_payload = [
            "email" => "kingsley.davidakindele@gmail.com",
            "password" => "Password"
        ];

        $response = $this->json('post', '/api/user/login', $user_payload);

        $response->assertStatus(Response::HTTP_OK);
    }
}
